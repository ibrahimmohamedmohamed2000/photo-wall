//
//  ApriFotocameraViewController.swift
//  ARDice
//
//  Created by IFTS07 on 29/03/22.
//

import UIKit

class ApriFotocameraViewController: UIViewController
{

    //unwind
    @IBOutlet weak var immagineScatto: UIImageView!
    
    @IBOutlet weak var tastoBottone1: UIButton!
    
    var getPhoto : ((UIImage) -> Void)?
    
    @IBOutlet weak var tastoBottone2: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        immagineScatto.backgroundColor = .secondarySystemBackground
        tastoBottone1.backgroundColor = .systemBlue //sfondo del bottone
        tastoBottone1.setTitle("Take Picture", for: .normal)
        tastoBottone1.setTitleColor(.white, for: .normal) //colore del testo
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if let image = immagineScatto.image
        {
            getPhoto?(image)
        }
    }
    
    
    @IBAction func actionBottone1(_ sender: Any)
    {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }
    
    @IBAction func ok(_ sender: UIButton)
    {
        dismiss(animated: true)
    }
    //
//    @IBAction func actionBottoneSelezionatoFoto(_ sender: UIButton)
//    {
//        self.performSegue(withIdentifier: "segueFotoScatto", sender: nil)
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ApriFotocameraViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    //abbaimo dissativato: una funzione incluso del delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    //l'immagine che vera scatta:
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        picker.dismiss(animated: true, completion: nil)
        
        //controlla l'immagine perche potrebbe essere nulla (che non ci sia)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else
        {
            return
        }
        immagineScatto.image = image
        
    }
    
}
