//
//  ArrayCornice.swift
//  ARDice
//
//  Created by IFTS07 on 04/04/22.
//

import Foundation
import UIKit

class ArrayCornice
{
    static var share : ArrayCornice = ArrayCornice()
    
    var arrayCorniceTotali : [DatiCornice] = [DatiCornice(_immaginiScelta: UIImage(named: "frame_01")!, _namecornice: "Frame01"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_02")!, _namecornice: "Frame02"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_03")!, _namecornice: "Frame03"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_04")!, _namecornice: "Frame04"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_05")!, _namecornice: "Frame05"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_06")!, _namecornice: "Frame06"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_07")!, _namecornice: "Frame07"),
                                              DatiCornice(_immaginiScelta: UIImage(named: "frame_08")!, _namecornice: "Frame08")]
    
}
