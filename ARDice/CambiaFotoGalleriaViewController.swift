//
//  CambiaFotoGalleriaViewController.swift
//  ARDice
//
//  Created by IFTS07 on 30/03/22.
//

import UIKit
import Photos
class CambiaFotoGalleriaViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{

    var imagePickerController = UIImagePickerController()
    
    @IBOutlet weak var fotoGalleria: UIImageView!
    
    @IBOutlet weak var tastoBottone_1: UIButton!
    
    @IBOutlet weak var tastoBottone_2: UIButton!
    
    var getPhoto_1 : ((UIImage) -> Void)?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        imagePickerController.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if let image_1 = fotoGalleria.image
        {
            getPhoto_1?(image_1)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func actionSelezionatoFotoGalleria(_ sender: Any)
    {
        self.imagePickerController.sourceType = .photoLibrary
        self.present(self.imagePickerController, animated: true, completion: nil)
    }
    
    //didFinishPickingMediaWithInfo = è una funzione per l'anteprima della fotocamera
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        fotoGalleria?.image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionFotoGalleria_Ok(_ sender: Any)
    {
        dismiss(animated: true)
    }
    
}
