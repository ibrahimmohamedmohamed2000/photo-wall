//
//  DatiCornice.swift
//  ARDice
//
//  Created by IFTS07 on 31/03/22.
//

import Foundation
import UIKit

class DatiCornice
{
    var immagineScelta : UIImage
    var namecornice : String
    init(_immaginiScelta : UIImage, _namecornice : String)
    {
        self.immagineScelta = _immaginiScelta
        self.namecornice = _namecornice
    }
    
}
