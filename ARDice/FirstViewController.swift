//
//  FirstViewController.swift
//  ARDice
//
//  Created by IFTS 23 on 29/03/22.
//

import UIKit
import Lottie

class FirstViewController: UIViewController {

    @IBOutlet weak var animationView: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lottieAnimation()
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timeToMoveOn), userInfo: nil, repeats: false)
    }

    @objc func timeToMoveOn() {
           self.performSegue(withIdentifier: "segue1", sender: self)
       }
    
    func lottieAnimation() {
        let animationView = AnimationView(name: "logo")
        animationView.frame = CGRect(x: 0, y: 0, width: 400, height: 700)
        animationView.center = self.view.center
        animationView.contentMode = .scaleAspectFit
        view.addSubview(animationView)
        animationView.play()
    }

}
