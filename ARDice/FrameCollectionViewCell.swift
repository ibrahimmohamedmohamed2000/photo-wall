//
//  FrameCollectionViewCell.swift
//  ARDice
//
//  Created by IFTS07 on 31/03/22.
//

import UIKit

class FrameCollectionViewCell: UICollectionViewCell
{
    @IBOutlet weak var imgCornice000: UIImageView!
    
    @IBOutlet weak var nomeTitoloCornice: UILabel!
}
