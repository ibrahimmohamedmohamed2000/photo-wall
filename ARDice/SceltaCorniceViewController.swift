//
//  SceltaCorniceViewController.swift
//  ARDice
//
//  Created by IFTS07 on 31/03/22.
//

import UIKit

class SceltaCorniceViewController: UIViewController, UICollectionViewDelegate,  UICollectionViewDataSource
{
    

    @IBOutlet weak var imgScelto: UIImageView!
    
    @IBOutlet weak var notSelezionare: UIButton!
    
    @IBOutlet weak var yesSelezionare: UIButton!
    
    @IBOutlet weak var titoloCornice: UILabel!
    
    @IBOutlet weak var corniceCollection: UICollectionView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return ArrayCornice.share.arrayCorniceTotali.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCornice", for: indexPath) as? FrameCollectionViewCell else { return UICollectionViewCell() }
        return cell
    }


}
