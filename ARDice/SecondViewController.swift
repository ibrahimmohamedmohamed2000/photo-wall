//
//  SecondViewController.swift
//  chaochao
//
//  Created by IFTS 23 on 17/03/22.
//

import UIKit

class SecondViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(timeToMoveOn), userInfo: nil, repeats: false)
    }

    @objc func timeToMoveOn() {
           self.performSegue(withIdentifier: "segue2", sender: self)
       }
}
