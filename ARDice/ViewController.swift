//
//  ViewController.swift
//  ARDice
//
//  Created by Ibrahim Mohamed on 26/02/22.
//

import UIKit
import SceneKit
import ARKit
import Photos

class ViewController: UIViewController, ARSCNViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    //aprire la galleria e bottoni del mini menu:
    
    var imagePickerController = UIImagePickerController()
    
    
    @IBOutlet weak var primoBottone: UIButton!
    
    @IBOutlet weak var secondoBottone: UIButton!
    
    @IBOutlet weak var terzoBottone: UIButton!
    
    @IBOutlet weak var quartoBottone: UIButton!
    
    @IBOutlet weak var miniView: UIView!
    
    @IBOutlet weak var bottonePlus: UIButton!
    
    
    //parte UIKIT:
    @IBOutlet var sceneView: ARSCNView!
    var arrayDot = [SCNNode]()
    var arrayFrames = [SCNScene]()
    var immagineScelta : UIImage?
    var frame: String?

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //parte Menu:
        miniView.isHidden = true
        bottonePlus.layer.cornerRadius = 7
       // controllarePermessoGalleria()
        imagePickerController.delegate = self
        
        
        // Set the view's delegate
        sceneView.delegate = self
        sceneView.autoenablesDefaultLighting = true
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        //        if let imageTotrack = ARReferenceImage.referenceImages(inGroupNamed: "Pokemon Cards", bundle: Bundle.main){
        //            configuration.trackingImages = imageTotrack
        //        }
        
        //        configuration.maximumNumberOfTrackedImages = 1
        
        // Run the view's session
        let tap = UITapGestureRecognizer(target: self, action: #selector(userTapped))
        sceneView.addGestureRecognizer(tap)
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    
    // MARK: - ARSCNViewDelegate
    
    /*
     // Override to create and configure nodes for anchors added to the view's session.
     func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
     let node = SCNNode()
     
     return node
     }
     */
//
//        func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
//
//            let node = SCNNode()
//            if let imageAnchor = anchor as? ARImageAnchor {
//                let plane = SCNPlane(width: imageAnchor.referenceImage.physicalSize.width, height: imageAnchor.referenceImage.physicalSize.height)
//                plane.firstMaterial?.diffuse.contents = UIColor(white: 1.0, alpha: 0.5)
//                let planeNode = SCNNode(geometry: plane)
//                planeNode.eulerAngles.x = -Float.pi / 2
//                node.addChildNode(planeNode)
//                if let pokeScene = SCNScene(named: "art.scnassets/eevee.scn") {
//                    if let pokeNode = pokeScene.rootNode.childNodes.first {
//                        pokeNode.eulerAngles.x =  Float.pi / 2
//                        planeNode.addChildNode(pokeNode)
//                    }
//                }
//            }
//            return node
//        }
    // da dove la metto con il dito nello spazio
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("began")
        guard let touch = touches.first else {return}
        print("result")
                let results = sceneView.hitTest(touch.location(in: sceneView), types: [ARHitTestResult.ResultType.featurePoint])
                guard let hitFeature = results.last  else { return}
        if #available(iOS 14, *){
            //
            addFrame(at: hitFeature)
        }else{
            addFrame(at: hitFeature)
        }
    }

    //il punto nello spazio da 2D a 3D
    func addFrame(at hitResult:ARHitTestResult)
    {
        if let scene = SCNScene(named: "art.scnassets/FrameOBJ.scn")
        {
//            print( frame.rootNode.childNodes.first?)
            print("added")
            let frame = scene.rootNode.childNode(withName: "FrameOBJ", recursively: true)
            //BARA-OUTSIDE-FOURI
            frame?.geometry?.materials[1].diffuse.contents = UIColor.black
            //GOWA-INSIDE-DENTRO
            
            frame?.geometry?.materials[0].diffuse.contents = immagineScelta ?? UIImage()
            
            let place = hitResult.worldTransform.columns.3
            // la posizione del frame nello spazio
            frame?.position = SCNVector3(place.x, place.y, place.z)
            // per fare girare il frame
            frame?.eulerAngles.y = .leastNormalMagnitude
            
        
            
//            scene.rootNode.position = SCNVector3(hitResult.worldTransform.columns.3.x, hitResult.worldTransform.columns.3.y, hitResult.worldTransform.columns.3.z)
            sceneView.scene.rootNode.addChildNode(frame!)
        }
        else
        {
            print("a7a")
        }
    }
    
    //
    @objc func userTapped(gesture:UITapGestureRecognizer){
//        let touchPosition = gesture.location(in: sceneView)
//        print(touchPosition)
//        let hitTest = sceneView.hitTest(touchPosition, types: .featurePoint)
//        if let position = hitTest.first {
//            print(position)
//            addFrame(at: position)
//        }
//
 
    }
    
    
    
    
    @IBAction func actionChangePhoto(_ sender: UIButton)
    {
        /*self.imagePickerController.sourceType = .photoLibrary
        self.present(self.imagePickerController, animated: true, completion: nil)
        */
        miniView.isHidden = true
        bottonePlus.isHidden = false
        sceneView.session.pause()
        performSegue(withIdentifier: "segueCambiaFoto", sender: nil)
    }
 
    
    func click_plus()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1))
        {
            self.miniView.layer.cornerRadius = 15
            self.miniView.isHidden = false
            self.bottonePlus.isHidden = true
        }
    }
    
    
    @IBAction func actionBottonePlus(_ sender: Any)
    {
        click_plus()
        
    }
    
    
    @IBAction func actionOpenTheCamera(_ sender: UIButton)
    {
        miniView.isHidden = true
        bottonePlus.isHidden = false
        sceneView.session.pause()
        performSegue(withIdentifier: "segueAprireFotocamera", sender: nil)
    }
    
    @IBAction func actionChangeFrame(_ sender: UIButton)
    {
        miniView.isHidden = true
        bottonePlus.isHidden = false
    }
    
    @IBAction func salvareFotoScattato(segue: UIStoryboardSegue)
    {
        let source  = segue.source as! ApriFotocameraViewController
        immagineScelta = source.immagineScatto.image
        let configuration = ARWorldTrackingConfiguration()
        //        if let imageTotrack = ARReferenceImage.referenceImages(inGroupNamed: "Pokemon Cards", bundle: Bundle.main){
        //            configuration.trackingImages = imageTotrack
        //        }
        
        //        configuration.maximumNumberOfTrackedImages = 1
        
        // Run the view's session
        let tap = UITapGestureRecognizer(target: self, action: #selector(userTapped))
        sceneView.addGestureRecognizer(tap)
        sceneView.session.run(configuration)
    }
    
    
    @IBAction func salvareFotoGalleria(segue : UIStoryboardSegue)
    {
        let source  = segue.source as! CambiaFotoGalleriaViewController
        immagineScelta = source.fotoGalleria.image
        let configuration = ARWorldTrackingConfiguration()
        //        if let imageTotrack = ARReferenceImage.referenceImages(inGroupNamed: "Pokemon Cards", bundle: Bundle.main){
        //            configuration.trackingImages = imageTotrack
        //        }
        
        //        configuration.maximumNumberOfTrackedImages = 1
        
        // Run the view's session
        let tap = UITapGestureRecognizer(target: self, action: #selector(userTapped))
        sceneView.addGestureRecognizer(tap)
        sceneView.session.run(configuration)
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueAprireFotocamera"
        {
            let vc = segue.destination as! ApriFotocameraViewController
            vc.getPhoto = {photo in
                print(photo)
                self.immagineScelta = photo
                self.sceneView.reloadInputViews()
            }
        }
        else if segue.identifier == "segueCambiaFoto"
        {
            let vc = segue.destination as! CambiaFotoGalleriaViewController
            vc.getPhoto_1 = {photo in
                print(photo)
                self.immagineScelta = photo
                self.sceneView.reloadInputViews()
            }
        }
    }
    
    
}
